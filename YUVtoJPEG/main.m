//
//  main.m
//  YUVtoJPEG
//
//  Created by grimmer on 2015/6/23.
//  Copyright (c) 2015年 grimmer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
