//
//  AppDelegate.h
//  YUVtoJPEG
//
//  Created by grimmer on 2015/6/23.
//  Copyright (c) 2015年 grimmer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

