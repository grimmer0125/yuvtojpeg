/**
 * 最简单的基于FFmpeg的图像编码器
 * Simplest FFmpeg Picture Encoder
 * 
 * 雷霄骅 Lei Xiaohua
 * leixiaohua1020@126.com
 * 中国传媒大学/数字电视技术
 * Communication University of China / Digital TV Technology
 * http://blog.csdn.net/leixiaohua1020
 * 
 * 本程序实现了YUV420P像素数据编码为JPEG图片。是最简单的FFmpeg编码方面的教程。
 * 通过学习本例子可以了解FFmpeg的编码流程。
 */

#include <stdio.h>
#import <Foundation/Foundation.h>

#define __STDC_CONSTANT_MACROS

//#ifdef _WIN32
////Windows
//extern "C"
//{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"

#import "h264Decoder.h"

#include "simplest_ffmpeg_picture_encoder.h"
//};
//#else
////Linux...
//#ifdef __cplusplus
//extern "C"
//{
//#endif
//#include <libavcodec/avcodec.h>
//#include <libavformat/avformat.h>
//#ifdef __cplusplus
//};
//#endif
//#endif

static FFMpegDecoder *h264decoder;

//ref: http://blog.csdn.net/leixiaohua1020/article/details/25346147

int initFFmpeg()
{
    [FFMpegDecoder staticInitialize];
    
    FFMpegDecoder *tmpffmpegdecoder = [[FFMpegDecoder alloc] initH264CodecWithWidth:0 height:0 privateData:nil];
  
    h264decoder = tmpffmpegdecoder;
//    self.h264decoder = tmpffmpegdecoder;
    
    return 0;
}

int testWriteFile()
{
    initFFmpeg();
    
    NSString *filePath1 = [[NSBundle mainBundle] pathForResource:@"0" ofType:@"h264"];
    NSData *htmlData1 = [NSData dataWithContentsOfFile:filePath1];
    
    int len1 = htmlData1.length;
    
    NSData *result1 = [h264decoder decodeFrame:htmlData1];

    NSString *filePath2 = [[NSBundle mainBundle] pathForResource:@"1" ofType:@"h264"];
    NSData *htmlData2 = [NSData dataWithContentsOfFile:filePath2];
    
    int len2 = htmlData2.length;
    
    NSData *result2 = [h264decoder decodeFrame:htmlData2];

    
    writeToFile(@"1.yuv", result2);
//    int kkk = 0;
    
    //NSData *dataImage = UIImageJPEGRepresentation(image, 0.8 );
    
    return 0;
    

    
}

//-(int)ttt
//{
//
//}

void writeToFile(NSString *fileName,NSData* saveData)
{
//    char chardata[4]={0,0,0,0};
//    NSData *dataImage =[NSData dataWithBytes:chardata length:4];
    
    NSString *docsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
//    NSDate *date = [NSDate date];
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"'opg'MMddyyyyHHmmss"];
//    NSString *fileName = @"test.data";//[NSString stringWithFormat:@"abc.jpg", [formatter stringFromDate:date]];
    
    NSString *imagePath = [docsPath stringByAppendingPathComponent:fileName];
    NSURL *imageUrl     = [NSURL fileURLWithPath:imagePath];
    if([saveData writeToURL:imageUrl atomically:YES])
    {
        int k =0;
    }
    else
    {
        int kk=0;
    }
    
    main2(saveData);
}


int main2 (NSData * yuvData) //int argc, char* argv[])
{
	AVFormatContext* pFormatCtx;
	AVOutputFormat* fmt;
	AVStream* video_st;
	AVCodecContext* pCodecCtx;
	AVCodec* pCodec;

	uint8_t* picture_buf;
	AVFrame* picture;
	AVPacket pkt;
	int y_size;
	int got_picture=0;
	int size;

	int ret=0;

	FILE *in_file = NULL;
    FILE *in_file2 =NULL; //YUV source
    
    int in_w=640,in_h=352; //480,in_h=272;                           //YUV's width and height
	
    
    NSString *docsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *outImagePath = [docsPath stringByAppendingPathComponent:@"1.jpg"];
    NSString *srcImagePath = [docsPath stringByAppendingPathComponent:@"1.yuv"];

    const char *src_file = [srcImagePath UTF8String];

    const char *out_file = [outImagePath UTF8String];
//    NSURL *imageUrl     = [NSURL fileURLWithPath:imagePath];
    const char* out_file2 = "cuc_view_encode.jpg";    //Output file

    
    
	in_file2 = fopen("cuc_view_480x272.yuv", "rb");
    in_file = fopen(src_file, "rb");

	av_register_all();

	//Method 1
	pFormatCtx = avformat_alloc_context();
	//Guess format
	fmt = av_guess_format("mjpeg", NULL, NULL);
    
    AVOutputFormat * fmt2 = av_guess_format("h264", NULL, NULL);

    
	pFormatCtx->oformat = fmt;
    //Output URL
	if (avio_open(&pFormatCtx->pb,out_file, AVIO_FLAG_READ_WRITE) < 0){
		printf("Couldn't open output file.");
		return -1;
	}

	//Method 2. More simple
	//avformat_alloc_output_context2(&pFormatCtx, NULL, NULL, out_file);
	//fmt = pFormatCtx->oformat;

	video_st = avformat_new_stream(pFormatCtx, 0);
	if (video_st==NULL){
		return -1;
	}
	pCodecCtx = video_st->codec;
	pCodecCtx->codec_id = fmt->video_codec;
	pCodecCtx->codec_type = AVMEDIA_TYPE_VIDEO;
	pCodecCtx->pix_fmt = PIX_FMT_YUVJ420P;

	pCodecCtx->width = in_w;  
	pCodecCtx->height = in_h;

	pCodecCtx->time_base.num = 1;  
	pCodecCtx->time_base.den = 25;   
	//Output some information
	av_dump_format(pFormatCtx, 0, out_file, 1);

	pCodec = avcodec_find_encoder(pCodecCtx->codec_id);
	if (!pCodec){
		printf("Codec not found.");
		return -1;
	}
	if (avcodec_open2(pCodecCtx, pCodec,NULL) < 0){
		printf("Could not open codec.");
		return -1;
	}
	picture = av_frame_alloc();
	size = avpicture_get_size(pCodecCtx->pix_fmt, pCodecCtx->width, pCodecCtx->height);
	picture_buf = (uint8_t *)av_malloc(size);
	if (!picture_buf)
	{
		return -1;
	}
	avpicture_fill((AVPicture *)picture, picture_buf, pCodecCtx->pix_fmt, pCodecCtx->width, pCodecCtx->height);

	//Write Header
	avformat_write_header(pFormatCtx,NULL);

	y_size = pCodecCtx->width * pCodecCtx->height;
	av_new_packet(&pkt,y_size*3);
    
	//Read YUV
	if (fread(picture_buf, 1, y_size*3/2, in_file) <=0)
	{
		printf("Could not read input file.");
		return -1;
	}
	picture->data[0] = picture_buf;  // 亮度Y
	picture->data[1] = picture_buf+ y_size;  // U 
	picture->data[2] = picture_buf+ y_size*5/4; // V

	//Encode
	ret = avcodec_encode_video2(pCodecCtx, &pkt,picture, &got_picture);
	if(ret < 0){
		printf("Encode Error.\n");
		return -1;
	}
	if (got_picture==1){
		pkt.stream_index = video_st->index;
		ret = av_write_frame(pFormatCtx, &pkt);
	}

	av_free_packet(&pkt);
	//Write Trailer
	av_write_trailer(pFormatCtx);

	printf("Encode Successful.\n");

	if (video_st){
		avcodec_close(video_st->codec);
		av_free(picture);
		av_free(picture_buf);
	}
	avio_close(pFormatCtx->pb);
	avformat_free_context(pFormatCtx);

	fclose(in_file);

	return 0;
}

